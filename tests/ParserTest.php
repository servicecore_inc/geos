<?php

namespace ServiceCore\Geos\Test;

use PHPUnit\Framework\TestCase;
use ServiceCore\Geos\Geometry\Point;
use ServiceCore\Geos\Geometry\Polygon;
use ServiceCore\Geos\Parser;

class ParserTest extends TestCase
{
    public function testParseWorksForSimplePolygon(): void
    {
        $geometry = Parser::parse('POLYGON((0 2, 2 2, 2 0, 0 0, 0 2))');

        $this->assertInstanceOf(Polygon::class, $geometry);
        $this->assertCount(5, $geometry->getValue());

        $vertices = $geometry->getValue();

        foreach ($vertices as $value) {
            $this->assertInstanceOf(Point::class, $value);
        }

        $this->assertEquals([0, 2], $vertices[0]->getValue());
        $this->assertEquals([2, 2], $vertices[1]->getValue());
        $this->assertEquals([2, 0], $vertices[2]->getValue());
        $this->assertEquals([0, 0], $vertices[3]->getValue());
        $this->assertEquals([0, 2], $vertices[4]->getValue());
    }
}
