<?php

namespace ServiceCore\Geos\Test\Geometry;

use PHPUnit\Framework\TestCase;
use ServiceCore\Geos\Geometry\Point;
use ServiceCore\Geos\Geometry\Polygon;

class PolygonTest extends TestCase
{
    public function testContainsWorksWithSimplePolygon(): void
    {
        $polygon = new Polygon([
            [0, 2],
            [2, 2],
            [2, 0],
            [0, 0],
            [0, 2]
        ]);

        $this->assertTrue($polygon->contains(new Point(1, 1)));
        $this->assertTrue($polygon->contains(new Point(1, 2)));
        $this->assertTrue($polygon->contains(new Point(0, 2)));
        $this->assertFalse($polygon->contains(new Point(3, 3)));
        $this->assertFalse($polygon->contains(new Point(-1, -1)));
    }

    public function testContainsWorksWithComplexPolygon(): void
    {

        $polygon = new Polygon([
            [-105.47973632813, 39.814453125],
            [-106.04003906251, 39.276123046875],
            [-105.69946289063, 38.902587890625],
            [-105.41381835938, 38.990478515625],
            [-105.61157226563, 39.2431640625],
            [-105.24902343751, 39.0673828125],
            [-104.98535156251, 39.22119140625],
            [-104.80957031251, 38.770751953125],
            [-104.28222656251, 39.188232421875],
            [-104.32617187501, 39.66064453125],
            [-104.53491210938, 40.1220703125],
            [-105.22705078126, 40.18798828125],
            [-105.62255859376, 40.111083984375],
            [-105.33691406251, 39.92431640625],
            [-105.47973632813, 39.814453125],
        ]);

        $this->assertTrue($polygon->contains(new Point(-105.26000976563, 40.067138671875)));
        $this->assertTrue($polygon->contains(new Point(-104.82055664063, 40.01220703125)));
        $this->assertTrue($polygon->contains(new Point(-105.85327148438, 39.276123046875)));
        $this->assertTrue($polygon->contains(new Point(-105.26000976563, 39.48486328125)));
        $this->assertTrue($polygon->contains(new Point(-105.43579101563, 39.00146484375)));
        $this->assertTrue($polygon->contains(new Point(-104.61181640626, 39.26513671875)));

        $this->assertFalse($polygon->contains(new Point(-104.62280273438, 40.396728515625)));
        $this->assertFalse($polygon->contains(new Point(-105.65551757813, 40.484619140625)));
        $this->assertFalse($polygon->contains(new Point(-106.04003906251, 40.05615234375)));
        $this->assertFalse($polygon->contains(new Point(-106.34765625001, 39.88037109375)));
        $this->assertFalse($polygon->contains(new Point(-106.31469726563, 39.583740234375)));
        $this->assertFalse($polygon->contains(new Point(-105.45776367188, 38.726806640625)));
        $this->assertFalse($polygon->contains(new Point(-104.97436523438, 38.551025390625)));
        $this->assertFalse($polygon->contains(new Point(-104.20532226563, 38.6279296875)));
        $this->assertFalse($polygon->contains(new Point(-103.88671875001, 39.385986328125)));
        $this->assertFalse($polygon->contains(new Point(-103.96362304688, 39.96826171875)));
        $this->assertFalse($polygon->contains(new Point(-104.62280273438, 40.396728515625)));
        $this->assertFalse($polygon->contains(new Point(-105.65551757813, 40.484619140625)));
        $this->assertFalse($polygon->contains(new Point(-106.04003906251, 40.05615234375)));
        $this->assertFalse($polygon->contains(new Point(-106.34765625001, 39.88037109375)));
        $this->assertFalse($polygon->contains(new Point(-105.51269531251, 39.1552734375)));
    }
}
