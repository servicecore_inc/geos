<?php

namespace ServiceCore\Geos;

use CrEOF\Geo\WKT\Parser as BaseParser;
use RuntimeException;
use ServiceCore\Geos\Geometry\Geometry;
use ServiceCore\Geos\Geometry\Point;
use ServiceCore\Geos\Geometry\Polygon;

class Parser
{
    // This function only current supports polygons
    public static function parse(string $wkt): Geometry
    {
        $parser   = new BaseParser();
        $geometry = $parser->parse($wkt);

        switch ($geometry['type']) {
            case 'POLYGON':
                if (\count($geometry['value']) > 1) {
                    throw new RuntimeException('Unable to parse polygons that contain other polygons.');
                }

                return new Polygon($geometry['value'][0]);
            default:
                throw new RuntimeException('Unable to parse provided wkt string.');
        }
    }
}
