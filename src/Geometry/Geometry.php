<?php

namespace ServiceCore\Geos\Geometry;

interface Geometry
{
    public function getValue();
}
