<?php

namespace ServiceCore\Geos\Geometry;

class Point implements Geometry
{
    private float $x;
    private float $y;

    public function __construct(float $x, float $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getValue(): array
    {
        return [$this->x, $this->y];
    }

    public function getX(): float
    {
        return $this->x;
    }

    public function getY(): float
    {
        return $this->y;
    }
}
