<?php

namespace ServiceCore\Geos\Geometry;

class Polygon implements Geometry
{
    private array $vertices;

    public function __construct(array $vertices)
    {
        foreach ($vertices as $vertex) {
            if (!$vertex instanceof Point) {
                $this->vertices[] = new Point($vertex[0], $vertex[1]);
            } else {
                $this->vertices[] = $vertex;
            }
        }
    }

    public function getValue(): array
    {
        return $this->vertices;
    }

    public function getVertices(): array
    {
        return $this->vertices;
    }

    // This point-in-polygon algoirthm was implemented using this paper by Kai Hormann and Alexander Agathos:
    // https://www.sciencedirect.com/science/article/pii/S0925772101000128
    public function contains(Point $point): bool
    {
        if ($point->getX() === $this->vertices[0]->getX() && $point->getY() === $this->vertices[0]->getY()) {
            return true; // vertex
        }

        $windingNumber = 0;

        for ($i = 0; $i < \count($this->vertices) - 1; $i++) {
            if (!\array_key_exists($i + 1, $this->vertices)) {
                break;
            }

            $currentPoint = $this->vertices[$i];
            $nextPoint = $this->vertices[$i + 1];

            if ($nextPoint->getY() === $point->getY()) {
                if ($nextPoint->getX() === $point->getX()) {
                    return true; // vertex
                }

                if ($currentPoint->getY() === $point->getY()
                    && (($nextPoint->getX() > $point->getX()) === ($currentPoint->getX() < $point->getX()))) {
                    return true; // edge
                }
            }

            if (($currentPoint->getY() < $point->getY()) !== ($nextPoint->getY() < $point->getY())) { // crossing
                if ($currentPoint->getX() >= $point->getX()) {
                    if ($nextPoint->getX() > $point->getX()) {
                        $windingNumber += 2 * (int)($nextPoint->getY() > $currentPoint->getY()) - 1;
                    } elseif ($this->rightCrossing($point, $currentPoint, $nextPoint)) { // right crossing
                        $windingNumber += 2 * (int)($nextPoint->getY() > $currentPoint->getY()) - 1;
                    }
                } elseif ($nextPoint->getX() > $point->getX()
                          && $this->rightCrossing($point, $currentPoint, $nextPoint)
                ) {
                    $windingNumber += 2 * (int)($nextPoint->getY() > $currentPoint->getY()) - 1;
                }
            }
        }

        return $windingNumber !== 0;
    }

    private function rightCrossing(Point $r, Point $p1, Point $p2): bool
    {
        return ($this->determinant($r, $p1, $p2) > 0) === ($p2->getY() > $p1->getY());
    }

    private function determinant(Point $r, Point $p1, Point $p2): float
    {
        return ($p1->getX() - $r->getX())*($p2->getY() - $r->getY())
               - ($p2->getX() - $r->getX())*($p1->getY() - $r->getY());
    }
}
